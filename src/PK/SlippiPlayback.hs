module PK.SlippiPlayback where

import Data.Aeson as Ae
import Data.List.NonEmpty (NonEmpty)
import System.IO.Temp
import System.Process
import Data.ByteString.Lazy qualified as BSL
import System.IO (hClose)
import Data.Maybe (catMaybes)

data SlippiComm =
  SlippiComm'Queue (NonEmpty SlippiQueueItem)
  deriving stock (Eq, Show)

instance ToJSON SlippiComm where
  toJSON = \case
    SlippiComm'Queue queue -> object ["mode" .= ("queue" :: String), "queue" .= queue]

data SlippiQueueItem = SlippiQueueItem
  { path :: FilePath
  , startFrame :: Maybe Int
  , endFrame :: Maybe Int
  , gameStartAt :: Maybe String
  , gameStation :: Maybe String
  } deriving stock (Eq, Show)

instance ToJSON SlippiQueueItem where
  toJSON SlippiQueueItem{..} = object $ catMaybes
    [ Just ("path" .= path)
    , fmap ("startFrame" .=) startFrame
    , fmap ("endFrame" .=) endFrame
    , fmap ("gameStartAt" .=) gameStartAt
    , fmap ("gameStation" .=) gameStation
    ]

data SlippiPlayback = SlippiPlayback
  { meleeISOPath :: FilePath
  , slippiPlaybackPath :: FilePath
  , comm :: SlippiComm
  } deriving stock (Eq, Show)


slippiPlayback :: SlippiPlayback -> IO ()
slippiPlayback SlippiPlayback{..} =
  withSystemTempDirectory "PK.SlippiPlayback.Config" $ \cfgDir ->
    withSystemTempFile "PK.SlippiPlayback.CommJSON" $ \jsonPath h -> do
      hClose h
      BSL.writeFile jsonPath (Ae.encode comm)
      callProcess slippiPlaybackPath [ "-i", jsonPath, "--exec", meleeISOPath, "-u", cfgDir ]
