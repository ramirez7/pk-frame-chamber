{-# LANGUAGE TupleSections #-}
module PK.Slippc where

import System.Directory (listDirectory)
import System.FilePath (isExtensionOf, dropExtension)
--import System.Process (readProcess)
import Data.Text (Text)
import Data.List qualified as List
import Data.Word
import Data.Aeson qualified as Ae
import Data.Aeson.Optics qualified as Ae
import UnliftIO.Async (pooledForConcurrentlyN)
import Optics
import Data.Traversable (for)
import Data.Map qualified as Map

fromCode
  :: forall a
   . Enum a
  => Bounded a
  => (a -> Word32)
  -> Word32
  -> Maybe a
fromCode toCode c = List.find ((== c) . toCode) [minBound..]

data Character =
   Mario
 | Fox
 | Falcon
 | DK
 | Kirby
 | Bowser
 | Link
 | Sheik
 | Ness
 | Peach
 | ICs
 | Pika
 | Samus
 | Yoshi
 | Puff
 | Mewtwo
 | Luigi
 | Marth
 | Zelda
 | YLink
 | Doc
 | Falco
 | Pichu
 | GNW
 | Ganon
 | Roy
 | MasterHand
 | WFMale
 | WFGirl
 | GigaBowser
 | Sandbag
 | CrazyHand
 | Popo
 deriving stock (Eq, Show, Enum, Bounded)

characterToCode :: Character -> Word32
characterToCode = \case
  Falcon -> 0x00
  DK -> 0x01
  Fox -> 0x02
  GNW -> 0x03
  Kirby -> 0x04
  Bowser -> 0x05
  Link -> 0x06
  Luigi -> 0x07
  Mario -> 0x08
  Marth -> 0x09
  Mewtwo -> 0x0A
  Ness -> 0x0B
  Peach -> 0x0C
  Pika -> 0x0D
  ICs -> 0x0E
  Puff -> 0x0F
  Samus -> 0x10
  Yoshi -> 0x11
  Zelda -> 0x12
  Sheik -> 0x13
  Falco -> 0x14
  YLink -> 0x15
  Doc -> 0x16
  Roy -> 0x17
  Pichu -> 0x18
  Ganon -> 0x19
  MasterHand -> 0x1B
  WFMale -> 0x1B
  WFGirl -> 0x1C
  GigaBowser -> 0x1D
  CrazyHand -> 0x1E
  Sandbag -> 0x1F
  Popo -> 0x20

characterFromCode :: Word32 -> Maybe Character
characterFromCode = fromCode characterToCode

data Stage =
   FoD
 | Pokemon
 | Yoshis
 | DL64
 | BF
 | FD
 deriving stock (Eq, Show, Enum, Bounded)

stageToCode :: Stage -> Word32
stageToCode = \case
  FoD -> 002
  Pokemon -> 003
  Yoshis -> 008
  DL64 -> 028
  BF -> 031
  FD -> 032

stageFromCode :: Word32 -> Maybe Stage
stageFromCode = fromCode stageToCode

data SlpMetaGame = SlpMetaGame
  { players :: [Character]
  , stage :: Stage
  } deriving (Eq, Show)

slpMetaGameFromJSON :: Ae.Value -> Either String SlpMetaGame
slpMetaGameFromJSON v = do
  stage <- maybe (Left "no stage") Right $ v ^? Ae.key "stage" % Ae._Integral >>= stageFromCode
  playerObj :: Map.Map Text Ae.Value <- maybe (Left "no metadata.players") Right $ v ^? Ae.key "metadata" % Ae.key "players" % Ae._JSON
  players <- fmap mconcat $ for (Map.toList playerObj) $ \(_, pv) -> do
    charObj :: Map.Map Word32 Int <- maybe (Left "no characters") Right $ pv ^? Ae.key "characters" % Ae._JSON
    for (Map.toList charObj) $ \(charCode, _) ->
      maybe (Left $ unwords ["Bad code", show charCode]) Right $ characterFromCode charCode
  pure SlpMetaGame{..}

slpMetaGameFromJSONFile :: FilePath -> IO SlpMetaGame
slpMetaGameFromJSONFile fp = Ae.eitherDecodeFileStrict' fp >>= \case
  Left err -> error $ unwords ["decodeEitherStrict'", err]
  Right j -> case slpMetaGameFromJSON j of
    Left err -> error $ unwords ["slpMetaGameFromJSON", err]
    Right smg -> pure smg

tagSlpFiles :: FilePath -> IO ()
tagSlpFiles dir = do
  slps <- filter ("slp" `isExtensionOf`) <$> (listDirectory dir)
  metas <- pooledForConcurrentlyN 8 slps $ \slp -> (slp,) <$> slpMetaGameFromJSONFile slp
  let _ = flip fmap metas $ \(slp, SlpMetaGame{..}) ->
        (slp, mconcat
          [ [dropExtension slp, "_"]
          , List.intersperse "-" $ 
            show stage : fmap show players
          ]
        )
  --TODO : Re-do parser to do the right thing for offline replays
  -- make sure to exclude chars without frames!
  pure ()
  
