module PK.Env where

import System.Environment
import Control.Monad.IO.Class

requireEnv :: MonadIO m => String -> m String
requireEnv e = liftIO (lookupEnv e) >>= \case
  Nothing -> error $ e ++ " not set!"
  Just s -> pure s

meleeISO :: MonadIO m => m FilePath
meleeISO = requireEnv "PK_MELEE_ISO"

slippiPlayback :: MonadIO m => m FilePath
slippiPlayback = requireEnv "PK_SLIPPI_PLAYBACK"
