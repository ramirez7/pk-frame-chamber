{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RecordWildCards #-}
module Main where

import Control.Monad (when)
import Data.List (sort)
import Data.List.NonEmpty (nonEmpty)
import PK.SlippiPlayback
import PK.Env qualified
import System.Environment (getArgs)
import System.IO (hIsTerminalDevice, stdin)

main :: IO ()
main = getArgs >>= \case
  ["watch"] -> do
    isTTY <- hIsTerminalDevice stdin
    when (isTTY) $ error "Please pipe replay paths over stdin"
    fmap (nonEmpty . lines) getContents >>= \case
      Nothing -> putStrLn "No slp files provides over stdin"
      Just files -> do
        let mkSQI file = SlippiQueueItem
              { path = file
              , startFrame = Just (-1)
              , endFrame = Nothing
              , gameStartAt = Nothing
              , gameStation = Nothing
              }
        meleeISOPath <- PK.Env.meleeISO
        slippiPlaybackPath <- PK.Env.slippiPlayback
        slippiPlayback SlippiPlayback
          { comm = SlippiComm'Queue $ fmap mkSQI files
          , ..
          }

  args -> error $ "Unknown args: " ++ show args
